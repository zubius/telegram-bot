package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"os"
	"strconv"
	"strings"
	"net/url"
	)

type User struct {
	Id int
	First_name string
	Last_name string
	Username string
}

type ResponseMe struct {
	Ok bool
	Description string
	Result User
}

type ResponseMessage struct {
	Ok bool
	Description string
	Result []Updates
}

type Updates struct {
	Update_id int
	Message Message
}

type Message struct {
	Message_id int
	From User
	Chat User
	Date int
	Text string
	Photo []PhotoSize
}

type PhotoSize struct {
	File_id string
	Width int
	Height int
	File_size int
}

var timeout string
var last_id int

func main() {
	token := os.Args[1]
	timeout := os.Args[2]
	/*resp, err := http.Get("https://api.telegram.org/bot"+token+"/getMe")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	res := &ResponseMe{}
	json.Unmarshal(body, &res)*/

	for {
		if last_id != -1 {
			last_id = getUpdates(token, timeout, last_id)
		} else {
			break
		}
	}
}

func getUpdates(token string, timeout string, last_id int) int {
	var URL = "https://api.telegram.org/bot"
	var SENDTEXT = "sendMessage"
	var GETUPD = "getUpdates"
	var SENDPHOTO = "sendPhoto"

	body := sendToApi(URL+token+"/"+GETUPD+"?offset="+strconv.Itoa(last_id)+"&timeout="+timeout)
	res := &ResponseMessage{}
	json.Unmarshal(body, &res)

	//fmt.Println(res)

	var local_last_id int

	for ind := range res.Result {

		in_text := res.Result[ind].Message.Text

		local_last_id = res.Result[ind].Update_id

		fmt.Println(local_last_id)

		if strings.HasPrefix(in_text, "/") {
			fmt.Println(in_text + " from " + res.Result[ind].Message.From.First_name + " "+res.Result[ind].Message.From.Last_name)
			switch in_text {
			case "/start":
			case "/help":
				sendToApi(URL+token+"/"+SENDTEXT+"?chat_id="+strconv.Itoa(res.Result[ind].Message.Chat.Id)+"&text="+
					"/stop Stops server side. You'll never get response till my Master will restart server.")
				last_id = local_last_id
			case "/stop":
				sendToApi(URL+token+"/"+SENDTEXT+"?chat_id="+strconv.Itoa(res.Result[ind].Message.Chat.Id)+"&text=Bot has stopped")

				sendToApi(URL+token+"/"+GETUPD+"?offset="+strconv.Itoa(local_last_id+1)+"&timeout=0")
				last_id = -2
			default:
				sendToApi(URL+token+"/"+SENDTEXT+"?chat_id="+strconv.Itoa(res.Result[ind].Message.Chat.Id)+"&text="+
					url.QueryEscape("Unknown command.\nTry to use /help"))
				last_id = local_last_id
			}
		} else {
			if len (in_text) > 0 {
				fmt.Println(in_text + " from " + res.Result[ind].Message.From.First_name + " "+res.Result[ind].Message.From.Last_name)
				out_text := "echo "+url.QueryEscape(in_text)
				sendToApi(URL+token+"/"+SENDTEXT+"?chat_id="+strconv.Itoa(res.Result[ind].Message.Chat.Id)+"&text="+out_text)
			}
			if len(res.Result[ind].Message.Photo) > 0 && len(res.Result[ind].Message.Photo[3].File_id) > 0 {
				photo := res.Result[ind].Message.Photo[3].File_id
				fmt.Println("Photo "+photo + " from " + res.Result[ind].Message.From.First_name + " "+res.Result[ind].Message.From.Last_name)
				sendToApi(URL+token+"/"+SENDPHOTO+"?chat_id="+strconv.Itoa(res.Result[ind].Message.Chat.Id)+"&photo="+photo)
			}
			last_id = local_last_id
		}
	}

	last_id++
	return last_id
}

func sendToApi (url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	return body
}